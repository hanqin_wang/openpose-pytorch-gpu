import cv2, sys, os, itertools
import numpy as np
import util
import math
import time
from scipy.ndimage.filters import gaussian_filter
import matplotlib.pyplot as plt
import matplotlib
from model import bodypose_model
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms
sys.path.append(os.path.expanduser("~/Documents"))
import omni_torch.visualize.basic as basic

class Body(object):
    def __init__(self, model_path, batch=1, img_size=(1080, 1920), channels=18, resize="input"):
        self.model = bodypose_model()
        self.resize = resize
        if torch.cuda.is_available():
            self.model = self.model.cuda()
        model_dict = util.transfer(self.model, torch.load(model_path))
        self.model.load_state_dict(model_dict)
        self.model.half()
        self.model.eval()
        self.create_gaussian_filter()
        self.create_offset_filter()
        self.limbSeq = torch.tensor([[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10],
                   [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17],
                   [1, 16], [16, 18], [3, 17], [6, 18]]).cuda() - 1
        # the middle joints heatmap correpondence
        self.mapIdx = torch.tensor([[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44], [19, 20], [21, 22],
                  [23, 24], [25, 26], [27, 28], [29, 30], [47, 48], [49, 50], [53, 54], [51, 52],
                  [55, 56], [37, 38], [45, 46]]).cuda() - 19
        self.indexer_y = torch.arange(img_size[0]).half().cuda().view(1, 1, -1, 1).repeat(batch, channels, 1, img_size[1])
        self.indexer_x = torch.arange(img_size[1]).half().cuda().view(1, 1, 1, -1).repeat(batch, channels, img_size[0], 1)
        self.indexer = torch.arange(img_size[0] * img_size[1]).cuda().int().view(1, 1, img_size[0], img_size[1]).repeat(batch, channels, 1, 1)
        print("initialize complete")

    def create_offset_filter(self, channels=18, kernel_size=3):
        one_location = [1, 7, 3, 5] # up down left right
        weights = []
        for loc in one_location:
            weight = torch.tensor([1 if _ == loc else 0 for _ in range(kernel_size ** 2)])
            #weight = weight.view(1, kernel_size, kernel_size).repeat(channels, 1, 1)
            weight = weight.view(kernel_size, kernel_size)
            weights.append(weight)
        #weights = torch.cat(weights, dim=0).unsqueeze(1)
        weights = torch.stack(weights, dim=0).repeat(channels, 1, 1).unsqueeze(1)
        self.offset_filter = nn.Conv2d(in_channels=channels, out_channels=len(one_location) * channels,
                                       kernel_size=kernel_size, groups=channels, bias=False, padding=1)
        self.offset_filter.weight.data = weights.half()
        self.offset_filter.weight.requires_grad = False
        self.offset_filter.half().cuda()

    def create_gaussian_filter(self, channels=18, kernel_size=25, sigma=3):
        pad_size = int((kernel_size - 1) / 2)

        # Create a x, y coordinate grid of shape (kernel_size, kernel_size, 2)
        x_cord = torch.arange(kernel_size)
        x_grid = x_cord.repeat(kernel_size).view(kernel_size, kernel_size)
        y_grid = x_grid.t()
        xy_grid = torch.stack([x_grid, y_grid], dim=-1)

        mean = (kernel_size - 1) / 2.
        variance = sigma ** 2.

        # Calculate the 2-dimensional gaussian kernel which is
        # the product of two gaussian distributions for two different
        # variables (in this case called x and y)
        exp = torch.exp(-torch.sum((xy_grid - mean).float() ** 2., dim=-1) / (2 * variance))
        gaussian_kernel = (1. / (2. * math.pi * variance)) * exp
        # Make sure sum of values in gaussian kernel equals 1.
        gaussian_kernel = gaussian_kernel / torch.sum(gaussian_kernel)

        # Reshape to 2d depthwise convolutional weight
        gaussian_kernel = gaussian_kernel.view(1, 1, kernel_size, kernel_size)
        gaussian_kernel = gaussian_kernel.repeat(channels, 1, 1, 1)

        self.gaussian_filter = nn.Conv2d(in_channels=channels, out_channels=channels,
                                    kernel_size=kernel_size, groups=channels, bias=False, padding=pad_size)

        self.gaussian_filter.weight.data = gaussian_kernel
        self.gaussian_filter.weight.requires_grad = False
        self.gaussian_filter.half().cuda()

    def parallel_linspace(self, coord, step=10):
        X = (coord[:, 2] - coord[:, 0]).unsqueeze(1)
        Y = (coord[:, 3] - coord[:, 1]).unsqueeze(1)
        N = (torch.arange(step).float() / (step - 1)).cuda(X.device.index).view(1, step)
        linspace = torch.stack((torch.matmul(X, N), torch.matmul(Y, N)), dim=-1)
        base = coord[:, :2].unsqueeze(1).repeat(1, 10, 1)
        return torch.round(base + linspace).long()


    def special_indexing(self, source, idx):
        shape = torch.tensor(list(source.shape)[1:] + [1]).cuda(source.device.index).unsqueeze(-1).repeat(idx.shape[0], 1)
        idx = torch.sum(idx * shape, dim=-1)
        return source.view(-1)[idx]

    def __call__(self, oriImg):
        # scale_search = [0.5, 1.0, 1.5, 2.0]
        scale_search = [0.5]
        boxsize = 1000
        stride = 8
        padValue = 128
        thre1 = 0.1
        thre2 = 0.05
        multiplier = [x * boxsize / oriImg.shape[0] for x in scale_search]
        # find connection in the specified sequence, center 29 is in the position 15
        limbSeq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10],
                   [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17],
                   [1, 16], [16, 18], [3, 17], [6, 18]]
        # the middle joints heatmap correpondence
        mapIdx = [[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44], [19, 20], [21, 22],
                  [23, 24], [25, 26], [27, 28], [29, 30], [47, 48], [49, 50], [53, 54], [51, 52],
                  [55, 56], [37, 38], [45, 46]]

        connection_all = []
        special_k = []
        mid_num = 10

        heat_inf = time.time()
        heatmap_avg, paf_avg = [], []
        with torch.no_grad():
            gpu_count_start = False
            for m in range(len(multiplier)):
                start = time.time()
                scale = multiplier[m]
                nearest_shape = [round(s * scale / stride) * stride for s in oriImg.shape]
                imageToTest_padded = cv2.resize(oriImg, (nearest_shape[1], nearest_shape[0]))
                im = np.transpose(np.float32(imageToTest_padded[:, :, :, np.newaxis]), (3, 2, 0, 1)) / 256 - 0.5
                im = np.ascontiguousarray(im)
                data = torch.from_numpy(im).half().cuda()
                usage = time.time() - start
                #print("\tCPU preprocessing time consumption: %.4f" % usage)

                if not gpu_count_start:
                    gpu_start = time.time()
                    gpu_count_start = True
                Mconv7_stage6_L1, Mconv7_stage6_L2 = self.model(data)
                _c = Mconv7_stage6_L2.size(1)
                b, c, h, w = Mconv7_stage6_L1.shape
                if self.resize == "original":
                    heatmap_avg.append(F.interpolate(Mconv7_stage6_L2[:, :-1, :, :],
                                                     size=(oriImg.shape[0], oriImg.shape[1]), mode='bicubic'))
                    paf_avg.append(F.interpolate(Mconv7_stage6_L1[:, self.mapIdx, :, :].squeeze(0),
                                                 size=(oriImg.shape[0], oriImg.shape[1]), mode='bicubic'))
                elif self.resize == "input":
                    heatmap_avg.append(F.interpolate(Mconv7_stage6_L2[:, :-1, :, :],
                                                     size=(data.shape[2], data.shape[3]), mode='bicubic'))
                    paf_avg.append(F.interpolate(Mconv7_stage6_L1[:, self.mapIdx, :, :].view(b * _c, 2, h, w),
                                                 size=(data.shape[2], data.shape[3]), mode='bicubic'))
                else:
                    heatmap_avg.append(Mconv7_stage6_L2[:, :-1, :, :])
                    paf_avg.append(Mconv7_stage6_L1[:, self.mapIdx, :, :].squeeze(0))

            heatmap_avg = torch.mean(torch.stack(heatmap_avg, dim=0), dim=0)
            paf_avg = torch.mean(torch.stack(paf_avg, dim=0), dim=0)
            if self.resize in ["original", "input"]:
                heatmap_avg = self.gaussian_filter(heatmap_avg)
            idx_thres = heatmap_avg > thre1
            b, c, h, w = heatmap_avg.shape
            hm = self.offset_filter(heatmap_avg).view(b, c, -1, h, w)
            idx = torch.sum(heatmap_avg.unsqueeze(2).repeat(1, 1, 4, 1, 1) >= hm, dim=2) == 4
            final_idx = idx * idx_thres
            #print("\tGPU time consumption: %.4f" % (time.time() - gpu_start))
            print("\theat estimation time consumption: %.4f" % (time.time() - heat_inf))

            # Toy implementation
            # This explained that the nonzero operation is not warmed up for every iteration
            start = time.time()
            tmp_i = idx.nonzero()
            print("\tnon zero time consumption: %.4f" % (time.time() - start))

            joint_inf = time.time()
            start = time.time()
            peak_idx = final_idx.nonzero()
            print("\tnon zero time consumption: %.4f" % (time.time() - start))
            peak_idx[:, (0, 1, 3, 2)] = peak_idx
            peak_score = heatmap_avg[peak_idx[:, 0], peak_idx[:, 1], peak_idx[:, 3], peak_idx[:, 2]]
            peak_score = torch.cat([peak_idx.half(), peak_score.unsqueeze(1)], dim=1)
            id_seq = list(torch.sum(torch.sum(final_idx, dim=-1), dim=-1).squeeze().cpu().numpy())


            start = time.time()
            all_peaks = []
            peak_score = peak_score.float().cpu().numpy()
            count = np.expand_dims(np.arange(peak_score.shape[0]), axis=1)
            peak_score = np.concatenate((peak_score, count), axis=1)
            mul = np.array([oriImg.shape[0] / final_idx.shape[2], oriImg.shape[0] / final_idx.shape[2], 1, 1])
            for i in range(final_idx.size(1)):
                _p_idx = peak_score[peak_score[:, 1] == i][:, 2:] * mul
                #data = [tuple(d) for d in _p_idx[:, 2:]]
                all_peaks.append(_p_idx)
            print("\tpeak estimation time consumption: %.4f" % (time.time() - start))


            start = time.time()
            division, vecs, comb, num = [], [], [], []
            for limb in limbSeq:
                limb = [l - 1 for l in limb]
                _m = id_seq[limb[0]] * id_seq[limb[1]]
                comb += list(itertools.product(list(range(id_seq[limb[0]])), list(range(id_seq[limb[1]]))))
                num += list(itertools.product(list(all_peaks[limb[0]][:, -1]), list(all_peaks[limb[1]][:, -1])))
                division.append(_m)
                if _m > 0:
                    a = peak_idx[peak_idx[:, 1] == limb[0]][:, 2:]
                    b = peak_idx[peak_idx[:, 1] == limb[1]][:, 2:]
                    a = a.unsqueeze(1).repeat(1, b.size(0), 1)
                    b = b.unsqueeze(0).repeat(a.size(0), 1, 1)
                    vecs.append(torch.cat([a, b], dim=-1).view(-1, 4))
            if len(vecs) == 0:
                return None, None
            vecs = torch.cat(vecs, dim=0).float()
            linspace = self.parallel_linspace(vecs, step=mid_num)
            linspace = linspace.view(-1, 2)
            #division = list((id_seq[self.limbSeq][:, 0] * id_seq[self.limbSeq][:, 1]).cpu().numpy())
            vecs = (vecs[:, 2:] - vecs[:, :2]).half()
            norm = torch.norm(vecs, dim=1)
            vecs = vecs / norm.unsqueeze(-1).repeat(1, 2)
            comb = torch.tensor(comb)
            num = torch.tensor(num)
            print("\tlinspace time consumption: %.4f" % (time.time() - start))

            start = time.time()
            #_paf_avg = paf_avg.squeeze(0)
            start_idx = 0
            for k in range(len(mapIdx)):
                end_idx = start_idx + division[k]
                if end_idx - start_idx == 0:
                    start_idx = end_idx
                    special_k.append(k)
                    connection_all.append([])
                    continue
                _linspace = linspace[start_idx * mid_num:end_idx * mid_num]
                vec_xy = paf_avg[k, :, _linspace[:, 1], _linspace[:, 0]].view(2, -1, mid_num).permute(1, 2, 0)
                vec = vecs[start_idx:end_idx].unsqueeze(1).repeat(1, mid_num, 1)
                score_midpts = torch.sum(vec_xy * vec, dim=-1)
                dist_prior = 0.5 * final_idx.shape[2] / norm[start_idx:end_idx] - 1
                dist_prior[dist_prior > 0] = 0
                score_with_dist_prior = torch.mean(score_midpts, dim=1) + dist_prior
                mask = (torch.sum(score_midpts > thre2, dim=-1) > mid_num * 0.8) * (score_with_dist_prior > 0)
                #print(vec_xy.shape)
                candidate = torch.cat((num[start_idx:end_idx], score_with_dist_prior.unsqueeze(1).cpu().float(),
                                       comb[start_idx:end_idx].float()), dim=1)[mask].numpy()
                candidate = candidate[(candidate[:, 2]*-1).argsort()]

                _connection = np.zeros((0, 5))
                for c in candidate:
                    if (c[3] not in _connection[:, 3] and c[4] not in _connection[:, 4]):
                        #connection.append(c)
                        _connection = np.vstack([_connection, c])
                        if (len(_connection) >= min(id_seq[limbSeq[k][0] - 1], id_seq[limbSeq[k][1] - 1])):
                            break

                connection_all.append(_connection)
                start_idx = end_idx
                #break
            print("\tCPU inference 1 time consumption: %.4f" % (time.time() - start))

            start = time.time()
            # last number in each row is the total parts number of that person
            # the second last number in each row is the score of the overall configuration
            subset = -1 * np.ones((0, 20))
            candidate = np.array([item for sublist in all_peaks for item in sublist])

            for k in range(len(mapIdx)):
                if k not in special_k:
                    partAs = connection_all[k][:, 0]
                    partBs = connection_all[k][:, 1]
                    indexA, indexB = np.array(limbSeq[k]) - 1

                    for i in range(len(connection_all[k])):  # = 1:size(temp,1)
                        found = 0
                        subset_idx = [-1, -1]
                        for j in range(len(subset)):  # 1:size(subset,1):
                            if subset[j][indexA] == partAs[i] or subset[j][indexB] == partBs[i]:
                                subset_idx[found] = j
                                found += 1

                        if found == 1:
                            j = subset_idx[0]
                            if subset[j][indexB] != partBs[i]:
                                subset[j][indexB] = partBs[i]
                                subset[j][-1] += 1
                                subset[j][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]
                        elif found == 2:  # if found 2 and disjoint, merge them
                            j1, j2 = subset_idx
                            membership = ((subset[j1] >= 0).astype(int) + (subset[j2] >= 0).astype(int))[:-2]
                            if len(np.nonzero(membership == 2)[0]) == 0:  # merge
                                subset[j1][:-2] += (subset[j2][:-2] + 1)
                                subset[j1][-2:] += subset[j2][-2:]
                                subset[j1][-2] += connection_all[k][i][2]
                                subset = np.delete(subset, j2, 0)
                            else:  # as like found == 1
                                subset[j1][indexB] = partBs[i]
                                subset[j1][-1] += 1
                                subset[j1][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]

                        # if find no partA in the subset, create a new subset
                        elif not found and k < 17:
                            row = -1 * np.ones(20)
                            row[indexA] = partAs[i]
                            row[indexB] = partBs[i]
                            row[-1] = 2
                            row[-2] = sum(candidate[connection_all[k][i, :2].astype(int), 2]) + connection_all[k][i][2]
                            subset = np.vstack([subset, row])
            # delete some rows of subset which has few parts occur
            deleteIdx = []
            for i in range(len(subset)):
                if subset[i][-1] < 4 or subset[i][-2] / subset[i][-1] < 0.4:
                    deleteIdx.append(i)
            subset = np.delete(subset, deleteIdx, axis=0)
            print("\tCPU inference 2 time consumption: %.4f" % (time.time() - start))
            print("\tjoint estimation time consumption: %.4f" % (time.time() - joint_inf))
        return candidate, subset

if __name__ == "__main__":
    body_estimation = Body('../model/body_pose_model.pth')
    test_image = '../images/ski.jpg'
    oriImg = cv2.imread(test_image)  # B,G,R order
    candidate, subset = body_estimation(oriImg)
    canvas = util.draw_bodypose(oriImg, candidate, subset)
    plt.imshow(canvas[:, :, [2, 1, 0]])
    plt.show()
