import sys
sys.path.insert(0, 'python')
import cv2
import math, time, os
import model
import util
from hand import Hand
from body import Body
import matplotlib.pyplot as plt
import copy
import numpy as np

division = 4
img_size = (540, 960)
body_estimation = Body('model/body_pose_model.pth')
hand_estimation = Hand('model/hand_pose_model.pth')
test_video = os.path.expanduser("~/Videos/Office3.mp4")

def timing_video():
    video_capture = cv2.VideoCapture(test_video)
    ret, frame = video_capture.read()
    # Warm up PyTorch
    start = time.time()
    #frame = cv2.resize(frame, (800, 640))
    candidate, subset = body_estimation(frame)
    end = time.time() - start
    print("Warmup cost: %.2f seconds" % (end))
    i = 0
    while ret:
        ret, frame = video_capture.read()
        i += 1
        if i < 450:
            continue
        start = time.time()
        #frame = cv2.resize(frame, (800, 640))
        candidate, subset = body_estimation(frame)
        end = time.time() - start
        print("Process single frame cost: %.2f seconds" % (end))
        if candidate is None:
            continue
        canvas = copy.deepcopy(frame)
        canvas, coords = draw_bodypose(canvas, candidate, subset, crop_size=60, draw=True)
        cv2.imwrite(os.path.expanduser("~/Pictures/tmp_%s.jpg" % str(i).zfill(2)), canvas)

def draw_bodypose(canvas, candidate, subset, stickwidth = 4, crop_size=16, draw=False, draw_line=False):
    joint_name = ["nose", "neck", "r_sholder", "e_elbow", "r_wrist", "l_sholder", "l_elbow", "l_wrist",
                  "r_hip", "r_knee", "r_ankle", "l_hip", "l_knee", "l_foot", "r_eye", "l_eye", "r_ear", "l_ear"]

    limbSeq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10],
               [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17],
               [1, 16], [16, 18], [3, 17], [6, 18]]

    colors = [[255, 0, 0], [255, 85, 0], [255, 170, 0], [255, 255, 0], [170, 255, 0], [85, 255, 0], [0, 255, 0],
              [0, 255, 85], [0, 255, 170], [0, 255, 255], [0, 170, 255], [0, 85, 255], [0, 0, 255], [85, 0, 255],
              [170, 0, 255], [255, 0, 255], [255, 0, 170], [255, 0, 85]]
    effective_coord = {}
    for i in range(18):
        # The first dim of subset represent the person in the pictures
        # The second dim of sebset represent the joint of that person
        for n in range(len(subset)):
            index = int(subset[n][i])
            #print(index)
            if index == -1:
                continue
            x, y = candidate[index][0:2]
            if draw:
                cv2.circle(canvas, (int(x), int(y)), stickwidth, colors[i], thickness=-1)

            if x - (crop_size / 2) <= 0 or x + (crop_size / 2) >= canvas.shape[1] or \
                y - (crop_size / 2) <= 0 or y + (crop_size / 2) >= canvas.shape[0]:
                zzz=0
            else:
                effective_coord.update({i: [x, y]})
            cv2.putText(canvas, "%s" % (i),
                        (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1, cv2.LINE_AA)
            if float(candidate[index][2]) < 0.5:
                cv2.putText(canvas, "%s: %.2f" % (joint_name[i], float(candidate[index][2])),
                            (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1, cv2.LINE_AA)
            #cv2.imwrite(os.path.expanduser("~/Pictures/tmp_%s.jpg"%str(i).zfill(2)), canvas)
        if draw_line:
            for i in range(17):
                for n in range(len(subset)):
                    index = subset[n][np.array(limbSeq[i]) - 1]
                    if -1 in index:
                        continue
                    cur_canvas = canvas.copy()
                    Y = candidate[index.astype(int), 0]
                    X = candidate[index.astype(int), 1]
                    mX = np.mean(X)
                    mY = np.mean(Y)
                    length = ((X[0] - X[1]) ** 2 + (Y[0] - Y[1]) ** 2) ** 0.5
                    angle = math.degrees(math.atan2(X[0] - X[1], Y[0] - Y[1]))
                    polygon = cv2.ellipse2Poly((int(mY), int(mX)), (int(length / 2), stickwidth), int(angle), 0, 360, 1)
                    cv2.fillConvexPoly(cur_canvas, polygon, colors[i])
                    canvas = cv2.addWeighted(canvas, 0.4, cur_canvas, 0.6, 0)
        # plt.imsave("preview.jpg", canvas[:, :, [2, 1, 0]])
        # plt.imshow(canvas[:, :, [2, 1, 0]])
    if len(effective_coord.keys()) <= 10:
        #print("bad image")
        return canvas, None
    else:
        # get patches:
        return canvas, effective_coord

if __name__ == '__main__':
    timing_video()
