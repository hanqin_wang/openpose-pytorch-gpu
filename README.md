## pytorch-openpose

This is a improvement of Open-Pose Pytorch by transfering the CPU operation to GPU. 
For a 1920 x 1080 image, the inference time was improved from 3 seconds to 0.07 second on RTX 2080ti.

Notice: This implementation use half precision computation, so on the GPU with out
Tensor Processing Unit, it might be now as fast as the time above.

The original pytorch implementation is [openpose](https://github.com/CMU-Perceptual-Computing-Lab/openpose) including **Body and Hand Pose Estimation**, and the pytorch model is directly converted from [openpose](https://github.com/CMU-Perceptual-Computing-Lab/openpose) caffemodel by [caffemodel2pytorch](https://github.com/vadimkantorov/caffemodel2pytorch). You could implement face keypoint detection in the same way if you are interested in. Pay attention to that the face keypoint detector was trained using the procedure described in [Simon et al. 2017] for hands.

### Model Download
* [dropbox](https://www.dropbox.com/sh/7xbup2qsn7vvjxo/AABWFksdlgOMXR_r5v3RwKRYa?dl=0)
